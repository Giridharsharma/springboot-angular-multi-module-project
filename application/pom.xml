<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <parent>
        <artifactId>sample.multimodule</artifactId>
        <groupId>sample.multimodule</groupId>
        <version>0.0.1-SNAPSHOT</version>
    </parent>

    <artifactId>sample.multimodule.application</artifactId>
    <packaging>jar</packaging>
    <name>Project Module - Application</name>

    <description>
        This is the main module of the project. It contains the application class in which the main
        method is defined that is necessary to run the Spring Boot Application. It also contains
        application configuration properties, Controller, views, and resources.
    </description>

    <properties>
        <jackson.databind.version>2.9.5</jackson.databind.version>
        <io.swagger.version>1.5.3</io.swagger.version>
        <swagger.maven.plugin.version>3.1.8</swagger.maven.plugin.version>
        <api.title>My API</api.title>
        <version.sequence>v1</version.sequence>
        <main.basedir>${project.parent.basedir}</main.basedir>
    </properties>

    <dependencies>
        <!-- Project modules -->
        <dependency>
            <groupId>sample.multimodule</groupId>
            <artifactId>sample.multimodule.service-impl</artifactId>
            <version>${project.version}</version>
        </dependency>

        <!-- Spring Boot dependencies -->
        <dependency>
            <groupId>org.apache.tomcat.embed</groupId>
            <artifactId>tomcat-embed-jasper</artifactId>
            <scope>provided</scope>
        </dependency>

        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
            <scope>provided</scope>
        </dependency>

        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-webmvc</artifactId>
        </dependency>

        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-web</artifactId>
        </dependency>

        <dependency>
            <groupId>com.fasterxml.jackson.core</groupId>
            <artifactId>jackson-databind</artifactId>
            <version>${jackson.databind.version}</version>
        </dependency>

        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-thymeleaf</artifactId>
            <scope>provided</scope>
        </dependency>

        <!-- Swagger dependencies -->
        <dependency>
            <groupId>io.swagger</groupId>
            <artifactId>swagger-core</artifactId>
            <scope>compile</scope>
            <version>${io.swagger.version}</version>
            <exclusions>
                <exclusion>
                    <groupId>javax.ws.rs</groupId>
                    <artifactId>jsr311-api</artifactId>
                </exclusion>
            </exclusions>
        </dependency>
    </dependencies>

    <build>
        <plugins>
            <!-- Spring Boot plugins -->
            <plugin>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-maven-plugin</artifactId>
            </plugin>

            <plugin>
                <groupId>com.github.kongchen</groupId>
                <artifactId>swagger-maven-plugin</artifactId>
                <version>${swagger.maven.plugin.version}</version>
                <configuration>
                    <apiSources>
                        <apiSource>
                            <springmvc>true</springmvc>
                            <locations>
                                <!-- Controllers location -->
                                <location>sample.multimodule.web.controllers</location>
                                <location>${project.groupId}.sample.multimodule.domain</location>
                            </locations>
                            <schemes>http,https</schemes>
                            <host>127.0.0.1:8080</host>
                            <basePath>/api</basePath>
                            <info>
                                <title>${api.title}</title>
                                <version>${version.sequence}</version>
                                <description>OpenAPI spec file generation</description>
                                <termsOfService>https://www.github.com</termsOfService>
                                <contact>
                                    <email>ernestoacostabaracoa@gmail.com</email>
                                    <name>Ernesto Antonio Rodriguez Acosta</name>
                                </contact>
                                <license>
                                    <url>http://www.license.com</url>
                                    <name>License name</name>
                                </license>
                            </info>
                            <outputFormats>yaml</outputFormats>
                            <templatePath>${basedir}/templates/strapdown.html.hbs</templatePath>
                            <outputPath>${basedir}/generated/document.html</outputPath>
                            <swaggerDirectory>${project.build.directory}/generated/swagger-ui</swaggerDirectory>
                        </apiSource>
                    </apiSources>
                </configuration>
                <executions>
                    <execution>
                        <phase>compile</phase>
                        <goals>
                            <goal>generate</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>

            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-resources-plugin</artifactId>
                <executions>
                    <execution>
                        <phase>compile</phase>
                        <goals>
                            <goal>copy-resources</goal>
                        </goals>
                        <configuration>
                            <outputDirectory>${main.basedir}/swagger-client-code/src/main/resources</outputDirectory>
                            <resources>
                                <resource>
                                    <directory>
                                        ${project.build.directory}/generated/swagger-ui
                                    </directory>
                                    <filtering>false</filtering>
                                </resource>
                            </resources>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>

</project>