package sample.multimodule.service.api;

import sample.multimodule.domain.dto.EmployeeDTO;
import sample.multimodule.domain.exception.ResourceNotFoundException;

import java.util.List;
import java.util.Map;

/**
 * This interface provides the service related
 * to account, such as <b>find</b> and <b>create</b>
 * an employee.
 *
 * @author Ernesto A. Rodriguez Acosta
 */
public interface EmployeeService {

    /**
     * Finds the account with the provided account number.
     * @return An employees list
     */
    List<EmployeeDTO> getAll();

    /**
     * Finds the employee with the provided employee id.
     * @param employeeId The employee id
     * @return The employee
     * @throws ResourceNotFoundException If the resource doesn't exists.
     */
    EmployeeDTO getEmployeeById(Long employeeId) throws ResourceNotFoundException;

    /**
     * Creates a new employee.
     * @param employee
     * @return The employee created.
     */
    EmployeeDTO createEmployee(EmployeeDTO employee);

    /**
     * Update an employee.
     * @param employeeId The employee id.
     * @param employeeDetails The employee data to be updated.
     * @return The employee updated.
     * @throws ResourceNotFoundException If the resource doesn't exists.
     */
    EmployeeDTO updateEmployee(Long employeeId, EmployeeDTO employeeDetails)
            throws ResourceNotFoundException;

    /**
     * Deletes an employee.
     * @param employeeId The employee id.
     * @return
     * @throws ResourceNotFoundException If the resource doesn't exists.
     */
    Map<String, Boolean> deleteEmployee(Long employeeId) throws ResourceNotFoundException;
}
