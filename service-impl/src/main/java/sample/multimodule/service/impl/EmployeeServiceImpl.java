package sample.multimodule.service.impl;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sample.multimodule.domain.dto.EmployeeDTO;
import sample.multimodule.domain.entity.Employee;
import sample.multimodule.repository.EmployeeRepository;
import sample.multimodule.service.api.EmployeeService;
import sample.multimodule.domain.exception.ResourceNotFoundException;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    @Autowired
    EmployeeRepository employeeRepository;

    /**
     * Finds the account with the provided account number.
     * @return An employees list
     */
    @Override
    public List<EmployeeDTO> getAll() {
        List<Employee> employees = (List<Employee>) employeeRepository.findAll();

        // Conversion type
        Type listType = new TypeToken<List<EmployeeDTO>>() {}.getType();

        return new ModelMapper().map(employees, listType);
    }

    /**
     * Finds the employee with the provided employee id.
     * @param employeeId The employee id
     * @return The employee
     * @throws ResourceNotFoundException If the resource doesn't exists.
     */
    @Override
    public EmployeeDTO getEmployeeById(Long employeeId) throws ResourceNotFoundException {
        Employee employee = employeeRepository.findById(employeeId)
                .orElseThrow(() -> new ResourceNotFoundException("Employee not found for this id: " +
                        employeeId));

        return new ModelMapper().map(employee, EmployeeDTO.class);
    }

    /**
     * Creates a new employee.
     * @param employee
     * @return The employee created.
     */
    @Override
    public EmployeeDTO createEmployee(EmployeeDTO employee) {
        ModelMapper modelMapper = new ModelMapper();

        Employee storedEmployee = employeeRepository.save(modelMapper.map(employee, Employee.class));

        return modelMapper.map(storedEmployee, EmployeeDTO.class);
    }

    /**
     * Update an employee.
     * @param employeeId The employee id.
     * @param employeeDetails The employee data to be updated.
     * @return The employee updated.
     * @throws ResourceNotFoundException If the resource doesn't exists.
     */
    @Override
    public EmployeeDTO updateEmployee(Long employeeId, EmployeeDTO employeeDetails)
            throws ResourceNotFoundException {
        ModelMapper modelMapper = new ModelMapper();

        Employee employee = employeeRepository.findById(employeeId)
                .orElseThrow(() -> new ResourceNotFoundException("Employee not found for this id: " +
                        employeeId));

        employee = (Employee) modelMapper.typeMap(EmployeeDTO.class, Employee.class)
                .addMappings(mapper -> {
                    mapper.skip(Employee::setId);
                });

        final Employee updatedEmployee = employeeRepository.save(employee);

        return modelMapper.map(updatedEmployee, EmployeeDTO.class);
    }

    /**
     * Deletes an employee.
     * @param employeeId The employee id.
     * @return
     * @throws ResourceNotFoundException If the resource doesn't exists.
     */
    @Override
    public Map<String, Boolean> deleteEmployee(Long employeeId) throws ResourceNotFoundException {

        Employee employee = employeeRepository.findById(employeeId)
                .orElseThrow(() -> new ResourceNotFoundException("Employee not found for this id: " +
                        employeeId));

        employeeRepository.delete(employee);

        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);

        return response;

    }
}
