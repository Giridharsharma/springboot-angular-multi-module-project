package sample.multimodule.domain.entity;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "employees")
@Setter
@Getter
@RequiredArgsConstructor
@ToString
public class Employee {

    @Id
    private Long id;
    private String firstName;
    private String lastName;
    private String emailId;
}
