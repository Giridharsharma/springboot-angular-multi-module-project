package sample.multimodule.domain.beans.error;

import lombok.Getter;

import java.util.Date;

/**
 * This class is used to define a specific
 * error response structure.
 *
 * @author Ernesto A. Rodriguez Acosta
 */
@Getter
public class ErrorDetails {

    private Date timestamp;
    private String message;
    private String details;

    public ErrorDetails(Date date, String message, String description) {
        super();
        this.timestamp = date;
        this.message = message;
        this.details = description;
    }
}
